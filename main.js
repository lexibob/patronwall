﻿url = "https://script.google.com/macros/s/AKfycbyqsglvq3LkQECpai2Wv42AE8M51RU23ZoHF85cCbpDETdBYHY/exec";


$(function() {

    update();
    
    setInterval(update, 1000 * 60);
    
    
});
  
  function update()
  {
       $.get(url, function(data){
            updateDisplay(data);
       })
  }


  function updateDisplay(contents) {
    var list = $("#games-list");
    
    list.html("");

    contents = JSON.parse(contents);
    for (var i = 0; i < contents.length; i++) {
      var game = contents[i];
      var item = $("<div/>");
      item.append($("<h2/>").text(game.nameOnBoard));
      item.addClass("game");
      // item.append("<div style='clear: both'></div>");
      
      list.append(item);  
    }
  }  